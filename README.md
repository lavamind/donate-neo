# Donate Neo

Donate neo is a rewrite of the [donate middleware][] and [donate-static][] using Django.

Due to bugs and maintainability problems with the original middleware and static frontend, [it was decided][] that a "unified" middleware + frontend web application was the best solution. Donate neo is not a complete application by itself, and relies on CiviCRM APIs. Donate neo's job is to sit in front of the CiviCRM instance and act as a "bastion", calling CiviCRM APIs with sanitized requests and without user input, as well as presenting a nice to look at donation frontend for donors.

[donate middleware]: https://gitlab.torproject.org/tpo/web/donate
[donate-static]: https://gitlab.torproject.org/tpo/web/donate-static
[it was decided]: https://gitlab.torproject.org/tpo/web/donate-static/-/issues/107

This readme is written with the goal of onboarding new developers, particularly people who may have never worked with Django before. If you're experienced with Django you can skip some of it, but there's some important bits of information here and there that you'll want to read.

## Getting started

Setting up a development environment is fairly simple, and just follows the standard Django project dev setup with a few extra steps.

1. [Install poetry](https://python-poetry.org/docs/)
2. Optionally install [flite](https://github.com/festvox/flite) and [sox](https://sox.sourceforge.net/) for audio captchas
3. From the project root, run the following commands

```sh
poetry install --extras=dev
cp settings_private.example.py tordonate/settings_private.py
poetry run ./manage.py makemigrations
poetry run ./manage.py migrate
poetry run ./manage.py createsuperuser
```

This installs dependencies, sets up a sqlite database, and prompts for a username/password for your new admin account. An insecure password is fine, since it's just a local dev setup.

After that, just run `poetry run ./manage.py runserver` and visit <http://localhost:8000>

## Compiling front-end code

Source files for front-end SASS and JS can be found in `static/sass` and `static/js`. Compiling and processing the CSS and JS is managed via `npm` scripts; NPM itself is managed via `nvm`.

1. [Install nvm](https://github.com/nvm-sh/nvm)
2. From the project root, run `nvm use`
3. If so prompted, run `nvm install lts/*`
4. Run `npm run build:js` or `npm run build:sass` to build JS or SASS directly into the web-facing folders at `tordonate/static/`.
5. Run `npm run watch` to watch source files with [onchange](https://www.npmjs.com/package/onchange) and process JS and SASS when they're modified.

## Contributing

Thanks for your interest! See the [contributing guide][] for how to get started :)

[contributing guide]: https://gitlab.torproject.org/tpo/web/donate-neo/-/blob/main/CONTRIBUTING.md

## License

This repository is licensed under the AGPL 3.0 license. Please see the LICENSE file in this repository for more information.
