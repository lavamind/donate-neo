import { initLocations, filterStates } from "./modules/locations.js";
import {
  stripeInit,
  stripeUpdate,
  stripeSubscriptionUpdate,
  stripeSubmit,
  stripeResults,
  haveStripeClientSecret,
} from "./modules/stripe.js";
import {
  paypalInit,
  paypalDonationSet,
  setupPaypalOneTimeButtons,
  setupPaypalSubscriptionButtons,
  paypalResults,
} from "./modules/paypal.js";

/* namespace code dependent on DOM */
var dTor = dTor || {};

dTor.baseUrl = window.location.origin;
dTor.page = $("body").attr("id") || "";
dTor.csrfToken = $("[name=csrfmiddlewaretoken]").val();
dTor.minimumDonation = document.getElementById('minimum-donation-data')
  ? JSON.parse(document.getElementById('minimum-donation-data').textContent)
  : {};

/*
 * function: init
 * purpose:
 *    perform DOM manipulation and
 *    bind interactions after
 *    DOM loads
 * attrs: n/a
 */

dTor.init = function () {
  // Initialize country and state location data
  // 233 is the magic ID number for USA
  initLocations(233);

  this.refreshCaptcha = function () {
    $.getJSON("/captcha/refresh/", function (result) {
      $("img.captcha").attr("src", result.image_url);
      $("#id_captcha_0").attr("value", result.key);
      $("#id_captcha_1").val("");
    });
  };

  /*
   * function: displayPaymentError
   * purpose:
   *    handle errors encountered
   *
   */
  this.displayPaymentError = function (message) {
    $("form.was-validated").removeClass("was-validated");
    dTor.refreshCaptcha();
    const messageContainer = $("#result-message");
    messageContainer.html(message).removeClass("hidden");
  };

  this.hidePaymentError = function() {
    const messageContainer = $("#result-message");
    messageContainer.html("").addClass("hidden");
  }

  this.handleFormErrors = function (errors) {
    $("form.was-validated").removeClass("was-validated");
    dTor.refreshCaptcha();
    for (const [key, value] of Object.entries(errors)) {
      switch (key) {
        case "captcha":
          $("#id_captcha_1").addClass("is-invalid");
          break;
        default:
          $(`[name='${key}']`).addClass("is-invalid");
          break;
      }
    }
  };
  

  /*
   * function: setupFormValidation
   * purpose:
   *    set up donation form
   *    validation for common
   *    issues before it gets to
   *    backend.
   * attrs: n/a
   */
  this.setupFormValidation = function () {
    dTor.hidePaymentError();
    $("form").on("submit", function (e) {
      if (
        !this.checkValidity() ||
        $(e.currentTarget).data("shippingRestricted")
      ) {
        e.preventDefault();
        e.stopPropagation();
        $.fn.matchHeight._update();
      }

      $(this).addClass("was-validated");
      dTor.beginPaymentProcess(e);
    });
  };

  
  this.validateElement = function (el) {
    if (!el.checkValidity() || $(el).val() == null) {
      $(el).siblings(".invalid-feedback").html(el.validationMessage);
      $(el).addClass("is-invalid");
    } else {
      $(el).removeClass("is-invalid");
    }
  };


  this.donationFormSyncInput = function (e) {
    let _this = $(this);
    let thisType = _this.attr("type");
    if (thisType == "button") return;

    if (_this.attr("required")) {
      dTor.validateElement(this);
    }
  };


  this.donationFormSyncSelect = function (e) {
    let _this = $(this); 
    let _thisLabel = $(this).siblings("label");
    
    if (_this.attr("required") || _thisLabel.hasClass("required")) {
      dTor.validateElement(this);
    }
  };


  /*
   * function: propagateDonation
   * purpose:
   *    when donation value changes,
   *    propagate implications of new value
   *    across form
   * attrs:
   *    d: donation value (in cents)
   */
  this.propagateDonation = function (d) {
    var dShow = "$" + d / 100;
    var frequency = $("#pricegrid").data("frequency") || "single";

    /* update stripe paymentIntent, if it exists */
    if (haveStripeClientSecret()) {
      if (parseInt(d) >= dTor.minimumDonation.cents) {
        if(frequency == "single") {
          stripeUpdate(d);
        } else {
          stripeSubscriptionUpdate(d);
        }
      };
    }

    /* update paypal order */
    if (parseInt(d) >= dTor.minimumDonation.cents) paypalDonationSet(d);

    /* ensure valid, and only valid, perks are selectable and selected */
    if ($("#noPerkCheckbox")) {
      $(".Perk-selection").each(function (i, el) {
        var _el = $(el);
        if (_el.data("price-tier-" + frequency) <= d) {
          _el.find("input[type='radio']").removeAttr("disabled");
          _el.removeClass("Perk-inactive").addClass("Perk-active");
        } else {
          _el
            .find("[id^='id_perk']")
            .prop("checked", false)
            .attr("disabled", "disabled");
          _el.removeClass("Perk-active").addClass("Perk-inactive");
        }
      });
    }

    /* Customize BTCPay submit button copy */
    if ($("#cryptoContainer").length) {
      if (!$("#secBTCPay").hasClass("hidden")) {
        var buttonCopy = $("#btn-donateBTCPay")[0];
        buttonCopy.innerHTML = "Donate <span>" + dShow + "</span> by BTCPay";
      }
    }
  };

  /* toggle perk option visibility given relevant perk ID */
  this.togglePerks = function (perkId) {
    $(".perk_option").each(function (i, el) {
      let _el = $(el);
      if (perkId == _el.data("perk")) {
        _el.removeClass("hidden");
        _el.find("select").attr("required", "required");
      } else {
        if (!_el.hasClass("hidden")) _el.addClass("hidden");
        _el.find("select").removeAttr("required");
      }
    });
  };

  

  this.beginPaymentProcess = function(e) {
    e.preventDefault();
    if(!$("#checkout_card").hasClass("hidden")) {
      stripeSubmit().then(function (errors) {
        if (errors) {
          dTor.handleFormErrors(errors);
        }
      });
    }
  }

  /* * * * * * * * * * * *
   * Interaction binding *
   * * * * * * * * * * * */
  
  /* per-field form validation */
  $(".donate-form")
  .on("change keyup", "input", this.donationFormSyncInput)
  .on("change keyup blur", "select", this.donationFormSyncSelect);


  /* Section toggle switches
     (e.g., Paypal vs Stripe forms; crypto QR codes vs BTCPay)
  */
  $(".sectionToggle").on("click", "[data-toggles]", function (e) {
    e.preventDefault();
    var _this = $(this);
    _this.addClass("active").siblings("[data-toggles]").removeClass("active");
    var targetID = _this.data("toggles");
    $(".toggleable").each(function () {
      if ($(this).is(targetID)) {
        $(this).removeClass("hidden");
      } else {
        $(this).addClass("hidden");
      }
    });
    /* ensure visible elements have been matchHeighted */
    $.fn.matchHeight._update();
  });

  /* Price grid donation frequency toggle switches
     (also manages default selection)
  */
  $(".frequencyToggle")
    .on("click", "[data-toggles_price]", function (e) {
      // e.preventDefault();
      var _this = $(this);
      var targetID = _this.data("toggles_price");
      var frequency = _this.data("toggles_to");
      var currentVal = $("#customDonation").val() * 100;

      /* manipulate radio button group visbility;
       maintain donation selection between categories */
      $(".Perk").removeClass("single monthly").addClass(frequency);
      $("#pricegrid")
        .data("frequency", frequency)
        .find(":checked")
        .removeAttr("checked");
      $(".toggleablePrice").each(function () {
        if ($(this).is(targetID)) {
          let checkableChildren = $(this).find(
            "input[value='" + currentVal + "']"
          );
          if (checkableChildren) {
            checkableChildren.prop("checked", true);
          }
          $(this).removeClass("hidden");
        } else {
          $(this).addClass("hidden");
        }
      });

      $("#checkout_paypal").removeClass("single monthly").addClass(frequency);

      /* ensure relevant fields update */
      dTor.propagateDonation(currentVal);

      /* ensure visible elements have been matchHeighted */
      $.fn.matchHeight._update();
    })
    .on("blur", "input[id^='customDonation']", function (e) {
      var _this = $(this);
      dTor.propagateDonation(_this[0].value * 100);
    });

  /* Sync donation input field with radio button values */
  $("[id^='pricegrid_']").on("click", "input[type='radio']", function (e) {
    var _this = $(this);
    var _parent = $("#pricegrid");
    var _customInput = _parent.find("input[id^='customDonation']");
    var donationAmount = _this[0].value;
    var donationDisplay = donationAmount / 100;

    _customInput[0].value = donationDisplay;
    dTor.propagateDonation(donationAmount);
  });

  /* Sync radio button values with donation input field */
  $("#pricegrid").on("change, keyup", "#customDonation", function (e) {
    var _this = $(this);
    var _parent = $("#pricegrid");
    var priceGroup = _parent.find(".toggleablePrice").not(".hidden");

    priceGroup.find("input[type='radio']").each(function (i, e) {
      var _e = $(e);
      if (_this[0].value * 100 == _e.val()) {
        _e.prop("checked", true);
        _e.attr("checked", "checked");
      } else {
        _e.prop("checked", false);
      }
    });
    /* ensure visible elements have been matchHeighted */
    $.fn.matchHeight._update();

    dTor.propagateDonation(_this[0].value * 100);
  });

  $(".Card-gift")
    /* Selecting "no perk" deselects perks */
    .on("click", "#noPerkCheckbox", function (e) {
      var _this = $(this);
      if (_this.prop("checked")) {
        $(".Card-gift").find("input[type='radio']").prop("checked", false);
        dTor.togglePerks(0);
      }
    })
    /* Selecting a perk deselects "no perks" */
    .on("click", "input[type='radio']", function (e) {
      var _this = $(this);
      if (_this.prop("checked")) {
        $("#noPerkCheckbox").prop("checked", false);
        dTor.togglePerks(_this.val());
      }
    });

  /* Refresh "state" form field options when selecting a country */
  $("form").on("change keyup blur", "#id_country", function (e) {
    var cName = $(e.currentTarget).val(),
      cOption = $(e.currentTarget).find('option[value="' + cName + '"]'),
      cId = cOption.data("countryid"),
      parentForm = $(e.currentTarget).closest("form");

    /* refresh location  */
    filterStates(cOption.data("countryid"));

    /* Disable form for restricted countries */
    if (cId == 182 || cId == 230) {
      parentForm.find(".shippingRestricted").removeClass("hidden");
      parentForm.data("shippingRestricted", false);
    } else {
      if (!parentForm.find(".shippingRestricted").hasClass("hidden")) {
        parentForm.find(".shippingRestricted").addClass("hidden");
      }
      parentForm.data("shippingRestricted", true);
    }
  });


  /* click-to-copy button on crypto fields */
  $("#secCryptoAddresses")
    .on("click", ".currencyAddress--copy", function (e) {
      e.preventDefault();
      var _this = $(this);
      var address = _this.siblings(".currencyAddress").attr("value");
      navigator.clipboard.writeText(address);
    })
    .on("click", ".currencyAddress--openModal", function (e) {
      e.preventDefault();
      var _this = $(this);
    });

  /* FAQ "collapse-all" button */
  $(".Faq--Nav").on("click", "button", function (e) {
    $(".Faq--Content").find(".collapse").collapse("hide");
  });

  switch (dTor.page) {
    case "donate":
      /* instantiate Paypal buttons + scripting */
      setupPaypalOneTimeButtons();
      setupPaypalSubscriptionButtons();

      /* instantiate stripe object + DOM elems */
      stripeInit(12500, dTor.csrfToken, dTor.baseUrl, dTor.displayPaymentError, dTor.handleFormErrors);

      /* instantiate paypal buttons */
      paypalInit(12500, dTor.csrfToken, dTor.baseUrl, dTor.displayPaymentError, dTor.handleFormErrors);

      /* instantiate form validation */
      dTor.setupFormValidation();

      /* Instantiate after pageload with default selection */
      dTor.propagateDonation(12500);

      break;
    case "thankyou":
      stripeResults();
      paypalResults();
      break;
    case "crypto":
      break;
    case "faq":
      break;
    default:
      break;
  }
};

window.onload = dTor.init();
