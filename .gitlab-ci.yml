---
variables:
  SITE_NAME: donate-review.torproject.net
  TAG_LATEST: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:latest
  TAG_COMMIT: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
  CONTAINER_NAME: tordonate_${CI_COMMIT_REF_NAME}

.test-job:
  image: $TAG_COMMIT
  stage: test
  tags:
    - amd64
    - docker
  variables:
    POETRY_CACHE_DIR: .cache/pypoetry
  cache:
    key: pypoetry
    paths:
      - .cache/pypoetry
  before_script:
    - poetry install --no-root --only=dev --no-interaction --no-ansi
coverage:
  extends: .test-job
  script:
    - coverage run --source='.' manage.py test
    - coverage report -m

lint:
  extends: .test-job
  script:
    - flake8

mypy:
  extends: .test-job
  script:
    - mypy -p tordonate

deploy-review:
  stage: deploy
  rules:
    - if: "$SKIP_REVIEW_APPS != null"
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TITLE =~ /Merge branch.*/
      when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_COMMIT_REF_SLUG.$SITE_NAME
    on_stop: stop-review
    auto_stop_in: 1 week
  script:
    - |
      access_level="$(curl --header "PRIVATE-TOKEN: $CI_PROJECT_ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/members/$GITLAB_USER_ID" | jq .access_level)"
      if [ "$access_level" -ne 50 ]; then
        echo The user who started this CI job does not have permission to deploy
        echo "Required level: 50. Actual level: $access_level"
        exit 1
      fi
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - podman create --name=${CONTAINER_NAME} -p127.0.0.1::8000 $TAG_LATEST
    - podman generate systemd container-$CONTAINER_NAME > $HOME/.config/systemd/user/container-${CONTAINER_NAME}.service
    - systemctl --user daemon-reload
    - systemctl --user enable --now container-${CONTAINER_NAME}.service
    - sudo generate_reviewapp_proxies

stop-review:
  stage: deploy
  rules:
    - if: "$SKIP_REVIEW_APPS != null"
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TITLE =~ /Merge branch.*/
      when: manual
  variables:
    GIT_STRATEGY: none
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  script:
    - kill -9 "$(cat gunicorn.pid)"
    - sudo bash /srv/donate-review.torproject.net/teardown-apache-config.sh "$CI_COMMIT_REF_SLUG" "$SITE_NAME"

build-container:
  stage: build
  image:
    name: quay.io/podman/stable
    docker:
      user: podman
  tags:
    - amd64
    - docker
    - tpa
  script:
    - export TMPDIR=$(pwd)/.tmp
    - podman build -t ${TAG_COMMIT} -t ${TAG_LATEST} .
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - podman push ${TAG_COMMIT}
    - podman push ${TAG_LATEST}
  cache:
    key: buildah-cache
    paths:
      - .tmp/buildah-cache-*
