/* * * * * * * * * * * *
 * Stripe integration  *
 * * * * * * * * * * * */

var paymentId = null;
var clientSecret = null;
var elements = null;
var paymentElement = null;
var subscriptionElement = null;
var csrfToken = null;
var baseUrl = null;
var displayPaymentError = null;
var handleFormErrors = null;
const unknownServiceError = `An unexpected error occurred.
Please wait and try your donation again later.
If this error persists, please email admin@torproject.net.`;
const paymentElementOptions = { layout: "tabs" };
const paymentElementAppearance = { theme: "stripe" };

// Initializes Stripe one-time payment, stashes client secret, and displays payment form
async function stripeInit(initialDonation, token, serverBase, errorPay, errorForm) {
  csrfToken = token;
  baseUrl = serverBase;
  displayPaymentError = errorPay;
  handleFormErrors = errorForm;

  const response = await fetch("/stripe/create_payment_intent/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": csrfToken,
    },
    body: JSON.stringify({
      donation: initialDonation,
    }),
  });

  let clientResponse = await response.json();
  if(clientResponse.error) {
    displayPaymentError(clientResponse.error);
    return false;
  }
  if(!clientResponse.client_secret) {
    displayPaymentError(unknownServiceError);
    return false;
  }

  clientSecret = clientResponse.client_secret;
  paymentId = clientResponse.payment_id;
  
  elements = stripe.elements({ paymentElementAppearance, clientSecret });
  paymentElement = elements.create("payment", paymentElementOptions);
  paymentElement.mount("#payment-element");
}

// Updates and displays Stripe one-time payment form
async function stripeUpdate(donationAmount) {
  const response = await fetch("/stripe/update_payment_intent/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": csrfToken,
    },
    body: JSON.stringify({
      payment_id: paymentId,
      donation: donationAmount,
    }),
  });
  let clientResponse = await response.json();
  
  if(clientResponse.error) {
    paymentElement.unmount()
    displayPaymentError(clientResponse.error);
    return false;
  }
  if(!clientResponse.client_secret) {
    displayPaymentError(unknownServiceError);
    return false;
  }

  if(subscriptionElement) {
    subscriptionElement.unmount();
  }

  elements = stripe.elements({ paymentElementAppearance, clientSecret });
  paymentElement = elements.create("payment", paymentElementOptions);
  paymentElement.mount("#payment-element");
}

// Updates and displays subscription payment form
async function stripeSubscriptionUpdate(donationAmount) {

  if(paymentElement) {
    paymentElement.unmount()
  }
  if(subscriptionElement != null) {
    subscriptionElement.destroy()
  }

  elements = stripe.elements({
    mode: 'subscription',
    amount: donationAmount,
    currency: 'usd',
    appearance: paymentElementAppearance,
  });
  subscriptionElement = elements.create("payment", paymentElementOptions);
  subscriptionElement.mount("#payment-element");
}

/*
 * function: stripeSubmit
 * purpose:
 *    sniff URL for stripe transaction
 *    parameters and conditionally
 *    display content based on result
 *    of transaction
 * attrs: e
 *    form submission action
 */

async function stripeSubmit() {
  const formData = $("#donateForm").serializeArray();
  const formParam = $.param(formData);

  const formValidate = await fetch("/validate/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": csrfToken,
    },
    body: JSON.stringify({
      form: formParam,
    }),
  });

  const validateData = await formValidate.json();

  if(validateData.errors) {
    handleFormErrors(validateData.errors);
    return false;
  }

  let processObject = {};

  formData.forEach(function (el) {
    processObject[el.name] = el.value;
  });

  processObject.cid = paymentId;
  processObject.email = processObject.email_address;
  processObject.amount = processObject.donation_amount;
  processObject.recurring = processObject.single_or_monthly == "monthly" ? true : false;
  
  /* When deferring the collection of client_secret, as we do with subscriptions, Stripe requires
     that we manually run form validation via the elements object in preparation for instantiating the
     subscription. We do so here. */
  if(processObject.recurring) {
    var elementsError = await elements.submit();
    if (elementsError.error) {
      displayPaymentError(elementsError.error);
      return;
    }
  }

  const stripeResponse = await fetch("/stripe/process/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": csrfToken,
    },
    body: JSON.stringify(processObject),
  });

  const processData = await stripeResponse.json();

  if(processData.error) {
    paymentElement.unmount()
    displayPaymentError(processData.error);
    return false;
  } else {
    var confirmPaymentArgs = {
      elements,
      confirmParams: {
        return_url: `${baseUrl}/donate-thank-you/`,
      },
    }
    if(processData.type == "subscription") {
      if(!processData.client_secret) {
        displayPaymentError(unknownServiceError);
        return false;
      }
      confirmPaymentArgs['clientSecret'] = processData.client_secret;
    }
    const confirmError = await stripe.confirmPayment(confirmPaymentArgs);

    if(confirmError.error) {
      if (confirmError.error === "card_error" || confirmError.error === "validation_error") {
        displayPaymentError(confirmError.message);
      } else {
        displayPaymentError(unknownServiceError);
      }
    }
  }

  return;
}

/*
 * function: stripeResults
 * purpose:
 *    sniff URL for stripe transaction
 *    parameters and conditionally
 *    display content based on result
 *    of transaction
 * attrs: n/a
 */

async function stripeResults() {
  const params = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop) => searchParams.get(prop),
  });

  let intent = params.payment_intent || "";
  let secret = params.payment_intent_client_secret || "";

  if (!secret) return;

  const { paymentIntent } = await stripe.retrievePaymentIntent(secret);

  $("body").addClass(paymentIntent.status || "requires_payment_method");
}

function haveStripeClientSecret() {
  return clientSecret ? true : false;
}

export {
  stripeInit,
  stripeUpdate,
  stripeSubscriptionUpdate,
  stripeSubmit,
  stripeResults,
  haveStripeClientSecret,
};
