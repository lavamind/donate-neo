/* * * * * * * * * * * * *
 * Location form fields  *
 * * * * * * * * * * * * */

var db = new loki("csc.db");

const countriesJSON = "/static/data/countries.json";
const statesJSON = "/static/data/states.json";

/* Build database of countries and states and refresh related form options */
async function initLocations($cid = null) {
  var countries = db.getCollection("countries");
  if (!countries) {
    let countries = db.addCollection("countries");
    let $country_select = $("[id^='id_country']");
    await fetch(countriesJSON)
      .then((response) => response.json())
      .then(async (data) => {
        await data.forEach((c) => {
          countries.insert(c);
          if (c.id == $cid) {
            $country_select.append(`
              <option value="${c.name}" data-countryid="${c.id}" selected="selected">${c.name}</option>
            `);
          } else {
            $country_select.append(`
              <option value="${c.name}" data-countryid="${c.id}">${c.name}</option>
            `);
          }
        });
      });
  }

  var states = db.getCollection("states");
  if (!states) {
    let states = db.addCollection("states");
    let $state_select = $("[id^='id_state']");
    await fetch(statesJSON)
      .then((response) => response.json())
      .then(async (data) => {
        if (data.length) {
          $state_select.html(`
            <option disabled="true" selected="selected" value="">Select a state</option>
          `);
        }
        await data.forEach((d) => {
          states.insert(d);
          if (d.country_id == $cid) {
            $state_select.append(`
              <option value="${d.name}">${d.name}</option>
            `);
          }
        });
      });
  }
}

/* Filter collection of states by country and refresh related form options */
async function filterStates($cid = null) {
  let statesColl = db.getCollection("states");
  let states = await statesColl.find({ country_id: parseInt($cid) });
  let $states = $("#id_state");
  $states.children("option").remove();
  if (states.length) {
    $states.html(
      "<option disabled='true' selected='selected' value=''>Select a state</option>"
    );
    await states.forEach((s) => {
      $states.append(`
        <option value="${s.name}">${s.name}</option>
      `);
    });
  } else {
    $states.html(
      "<option disabled='true' selected='selected'>No state needed</option>"
    );
  }
}

export { initLocations, filterStates };
