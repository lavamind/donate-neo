from dataclasses import dataclass

import django_rq
import redis


@dataclass
class RedisController:
    host: str
    port: int
    db: str
    password: str

    def __post_init__(self) -> None:
        self.__connection = redis.Redis(
            host=self.host,
            port=self.port,
            db=self.db,
            password=self.password
        )

        self.__rqConnection = django_rq.get_queue(
            'default',
            connection=self.__connection
        )

    async def queue(self, message: str, args: dict) -> django_rq.job:
        return self.__rqConnection.enqueue(message, args)
