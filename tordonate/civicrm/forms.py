import typing as t

from asgiref.sync import async_to_sync
from dependency_injector.wiring import Provide, inject
from django import forms
from django.core.exceptions import ValidationError

from ..containers import Container
from .repository import CivicrmRepositoryMock


class SubscriptionForm(forms.Form):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)

    @async_to_sync
    @inject
    async def subscription_request(
        self,
        civi: 'CivicrmRepositoryMock' = Provide[Container.civi],
    ) -> None:
        subscriptionInfo = {
            "first_name": self["first_name"],
            "last_name": self["last_name"],
            "email": self["email"]
        }
        try:
            await civi.subscription_request(subscriptionInfo)
        except Exception as e:
            print(e)
            raise e

        pass

    # TODO: determine why using Optional[Dict[str, Any]] doesn't match cleaned_data's type ("Any")
    def clean(self) -> t.Any:
        errors = []
        cleaned_data = super().clean()

        if cleaned_data.get('first_name') is None:
            errors.append(ValidationError('Please enter a first name.'))
        if cleaned_data.get('last_name') is None:
            errors.append(ValidationError('Please enter a last name.'))

        if len(errors):
            raise ValidationError(errors)
        return cleaned_data
