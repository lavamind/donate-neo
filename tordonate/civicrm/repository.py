import typing as t

from django.conf import settings

from ..redis import RedisController
from ..utils import WithSyncProtocol, with_sync
from .models import Perk

if t.TYPE_CHECKING:
    from ..forms import DonorInfoForm


class CivicrmRepositoryProtocol(t.Protocol):
    """Protocol for CiviCRM repository implementations.

    Django isn't entirely async-ready yet, but we're using async anyway to provide a smooth
    transition path. Sync views can call these methods with `asgiref.sync.async_to_sync`.
    """

    async def get_active_perks(self) -> list[Perk]: ...

    async def get_yec_total(self) -> int: ...

    async def is_yec(self) -> bool: ...

    async def minimum_donation(self) -> int: ...

    async def get_perk_by_id(self, perk_id: str) -> t.Optional[Perk]: ...

    async def report_donation(
        self,
        message: str,
        args: dict
    ) -> None: ...

    async def newsletter_signup(self, donation_form: 'DonorInfoForm') -> None: ...


class CivicrmRepositoryMock(CivicrmRepositoryProtocol, metaclass=WithSyncProtocol):
    def __init__(
        self,
        redis: RedisController,
        active_perks: list[Perk],
        yec_total: int,
        is_yec: bool,
        minimum_donation: int,
    ):
        self._redis = redis
        self._active_perks = active_perks
        self._yec_total = yec_total
        self._is_yec = is_yec
        self._minimum_donation = minimum_donation

        self.env = "dev" if settings.TESTING is True else "prod"

    @with_sync
    async def get_active_perks(self) -> list[Perk]:
        return self._active_perks

    # TODO: determine why mypy complains about get_active_perks_sync
    def get_active_perks_synced(self) -> list[Perk]:
        return self._active_perks

    @with_sync
    async def get_yec_total(self) -> int:
        return self._yec_total

    @with_sync
    async def is_yec(self) -> bool:
        return self._is_yec

    @with_sync
    async def minimum_donation(self) -> int:
        return self._minimum_donation

    @with_sync
    async def get_perk_by_id(self, perk_id: str) -> t.Optional[Perk]:
        for perk in await self.get_active_perks():
            if perk.id == perk_id:
                return perk

        return None

    @with_sync
    async def report_donation(self, message: str, args: dict) -> None:
        await self._redis.queue(message, args)
        return

    @with_sync
    async def newsletter_signup(self, donation_form: 'DonorInfoForm') -> None:
        return

    @with_sync
    async def subscription_request(self, subscriptionInfo: dict) -> None:
        return
