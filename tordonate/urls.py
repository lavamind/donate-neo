"""tordonate URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from .paypal import urls as paypal_urls
from .stripe import urls as stripe_urls
from .views import CryptocurrencyView, DonateFormView, DonateThankYouView, FAQView

urlpatterns = [
    path('', DonateFormView.as_view(), name='donate_form'),
    path('validate/', DonateFormView.ajax_validate, name='donate_form_validation'),
    path('cryptocurrency/', CryptocurrencyView.as_view(), name='cryptocurrency'),
    path('donate-thank-you/', DonateThankYouView.as_view(), name='donate_thank_you'),
    path('faq/', FAQView.as_view(), name='faq'),
    path('admin/', admin.site.urls),
    path('captcha/', include('captcha.urls')),
    path('paypal/', include(paypal_urls)),
    path('stripe/', include(stripe_urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
