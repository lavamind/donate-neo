import json
from unittest.mock import MagicMock

from django.test import Client, TestCase
from stripe.util import convert_to_stripe_object

from tordonate import container
from tordonate.civicrm.repository import CivicrmRepositoryMock
from tordonate.stripe import StripeController


class StripeTests(TestCase):
    def test_single_donation(self) -> None:
        civi_mock = MagicMock(spec=CivicrmRepositoryMock)
        stripe_mock = MagicMock(spec=StripeController)
        client = Client()
        with container.civi.override(civi_mock), container.stripe.override(stripe_mock):
            response = client.post(
                '/stripe/process/',
                content_type='application/json',
                follow=True,
                data={
                    'recurring': False,
                    'amount': 1000,
                    'email': 'foo@bar.com',
                },
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content, b'{"type": "payment"}')
            self.assertFalse(stripe_mock.get_or_create_monthly_plan.called)
            self.assertFalse(stripe_mock.create_customer.called)
            self.assertFalse(stripe_mock.create_subscription.called)

    def test_monthly_donation(self) -> None:
        client = Client()
        civi_mock = MagicMock(spec=CivicrmRepositoryMock)
        stripe_mock = MagicMock(spec=StripeController)
        f = open('./tordonate/tests/stripe_subscription.json')
        mock_sub_return = json.load(f)
        stripe_mock.create_subscription.return_value = convert_to_stripe_object(mock_sub_return)
        with container.civi.override(civi_mock), container.stripe.override(stripe_mock):
            response = client.post(
                '/stripe/process/',
                content_type='application/json',
                follow=True,
                data={
                    'recurring': True,
                    'amount': 1000,
                    'email': 'foo@bar.com',
                },
            )

            self.assertEqual(response.status_code, 200)
            self.assertTrue(stripe_mock.get_or_create_monthly_plan.called)
            self.assertTrue(stripe_mock.create_customer.called)
            self.assertTrue(stripe_mock.create_subscription.called)
            self.assertEqual(
                response.content,
                b'{"type": "subscription", '
                b'"client_secret": "pi_3P3J9HAPRiS9lYuI2eaQAITD_secret_006DezIT5s0yQzfErx8rBQVNF"}'
            )
