import typing as t
from unittest.mock import MagicMock

from django.test import Client, TestCase
from django.urls import reverse

from tordonate import container
from tordonate.civicrm.repository import CivicrmRepositoryMock
from tordonate.containers import Container
from tordonate.forms import DonorInfoForm


class DonorInfoFormTests(TestCase):
    data = {
        'donation_amount': 2500,
        'single_or_monthly': 'single',
        'no_gift': True,
        'first_name': 'first',
        'last_name': 'last',
        'street_address': 'street address',
        'apartment_number': 'apartment',
        'country': 'country',
        'city': 'city',
        'state': 'state',
        'postal_code': '90210',
        'email_address': 'a@a.a',
        'email_updates': False,
        # XXX: the captcha field is named captcha_0 and captcha_1. for some reason, there's two
        # of them. i have no idea why.
        'captcha_0': 'PASSED',
        'captcha_1': 'PASSED',
    }

    def setUp(self) -> None:
        container = Container()
        self.civi = container.civi()

    def test_form_validation(self) -> None:
        form = DonorInfoForm(data=self.data)
        self.assertTrue(form.is_valid())

    def test_form_invalid_donation_amount(self) -> None:
        form = DonorInfoForm(data={
            **self.data,
            'donation_amount': self.civi.minimum_donation_sync(),  # type: ignore
            'no_gift': False,
            'perk': self.civi.get_active_perks_sync()[0].id,  # type: ignore
        })
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('__all__'))

    def test_form_invalid_perk(self) -> None:
        form = DonorInfoForm(data={
            **self.data,
            'donation_amount': self.civi.minimum_donation_sync(),  # type: ignore
            'no_gift': False,
            'perk': '',
        })
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('__all__'))

    def test_form_monthly_donation(self) -> None:
        form = DonorInfoForm(data={
            **self.data,
            'single_or_monthly': 'monthly',
            'donation_amount': self.civi.get_active_perks_sync()[0].monthly_price,  # type: ignore
            'no_gift': False,
            'perk': self.civi.get_active_perks_sync()[0].id,  # type: ignore
        })
        self.assertTrue(form.is_valid())

    def test_no_perk_and_not_no_gift(self) -> None:
        form = DonorInfoForm(data={
            **self.data,
            'no_gift': False,
        })
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('__all__'))

    def test_perk_and_no_gift(self) -> None:
        form = DonorInfoForm(data={
            **self.data,
            'no_gift': True,
            'perk': self.civi.get_active_perks_sync()[0].id,  # type: ignore
            'donation_amount': self.civi.get_active_perks_sync()[0].single_price,  # type: ignore
        })
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('__all__'))

    def test_newsletter_signup(self) -> None:
        data: dict[str, t.Any] = {
            **self.data,
            'email_updates': True,
        }
        client = Client()
        civi_mock = MagicMock(spec=CivicrmRepositoryMock)

        with container.civi.override(civi_mock):
            client.post(reverse('donate_form'), data)
            self.assertTrue(civi_mock.newsletter_signup_sync.called)

    def test_newsletter_signup_false(self) -> None:
        client = Client()
        civi_mock = MagicMock(spec=CivicrmRepositoryMock)

        with container.civi.override(civi_mock):
            client.post(reverse('donate_form'), self.data)
            self.assertFalse(civi_mock.newsletter_signup_sync.called)
