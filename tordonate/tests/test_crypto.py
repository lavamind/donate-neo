import datetime

from django.test import TestCase

from tordonate.forms import CryptocurrencyForm


class CryptocurrencyTests(TestCase):
    data = {
        'anonymous_donation': True,
        'first_name': '',
        'last_name': '',
        'email_opt_in': False,
        'email_address': 'foo@bar.baz',
        'estimated_donation_date': datetime.date.today(),
        'currency_type': 'BTC',
        'currency_amount': 1,
    }

    def test_form_validation(self) -> None:
        form = CryptocurrencyForm(data=self.data)
        self.assertTrue(form.is_valid())

    def test_invalid_form_no_name(self) -> None:
        form = CryptocurrencyForm(data={**self.data, 'anonymous_donation': False})
        self.assertIsNotNone(form.errors.get('__all__'))

    def test_invalid_form_past_date(self) -> None:
        form = CryptocurrencyForm(data={**self.data, 'estimated_donation_date':
                                        datetime.date.today() - datetime.timedelta(days=1)})
        self.assertIsNotNone(form.errors.get('estimated_donation_date'))
