from dependency_injector.wiring import Provide, inject
from django.http import HttpRequest
from django.template.response import TemplateResponse
from django.utils.deprecation import MiddlewareMixin

from .civicrm.repository import CivicrmRepositoryProtocol
from .containers import Container


class CiviCrmMiddleware(MiddlewareMixin):
    @inject
    def process_template_response(
        self,
        request: HttpRequest,
        response: TemplateResponse,
        civi: CivicrmRepositoryProtocol = Provide[Container.civi],
    ) -> TemplateResponse:
        response.context_data['civicrm'] = {
            'active_perks': civi.get_active_perks_sync(),  # type: ignore
            'yec_total': civi.get_yec_total_sync(),  # type: ignore
            'is_yec': civi.is_yec_sync(),  # type: ignore
        }
        return response
