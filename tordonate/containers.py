# from uuid import uuid4

from dependency_injector import containers, providers
from django.conf import settings

from .civicrm.models import Perk
from .civicrm.repository import CivicrmRepositoryMock
from .paypal import PaypalController
from .redis import RedisController
from .stripe import StripeController


class Container(containers.DeclarativeContainer):
    redis: providers.Factory[RedisController] = providers.Factory(
        RedisController,
        settings.REDIS_SERVER,
        settings.REDIS_PORT,
        settings.REDIS_DB,
        settings.REDIS_PASSWORD
    )
    civi: providers.Factory[CivicrmRepositoryMock] = providers.Factory(
        CivicrmRepositoryMock,
        redis=redis,
        # XXX: this exists for testing, so i can slowly migrate to use the DI container
        active_perks=[
            Perk(
                id="4723d3f8-fe1d-4f19-8d19-358ece677ec1",
                name='sticker pack',
                desc='Get a collection of your favorite Tor stickers to decorate your stuff.',
                single_price=2500, monthly_price=1500,
                image_data='/static/images/fpo/stickerpack-1.png',
                options=[]
            ),
            Perk(
                id="dc1dc9a6-ca65-4aef-b223-87d270c0b97f",
                name='t-shirt',
                desc='Get this year&rsquo;s Powered by Privacy T-shirt.',
                single_price=7500, monthly_price=2500,
                image_data='/static/images/fpo/yec-tee-web.png',
                options=[
                    {"T22-RCF": {"T22-RCF-C01": "Small", "T22-RCF-C02": "Medium",
                                 "T22-RCF-C03": "Large", "T22-RCF-C04": "XL",
                                 "T22-RCF-C05": "2XL", "T22-RCF-C06": "3XL",
                                 "T22-RCF-C07": "4XL"}},
                    ],
            ),
            Perk(
                id="afe082b3-c681-492e-8919-851ef7a89e53",
                name='t-shirt pack',
                desc='Get this year&rsquo;s Tor Onions and Powered by Privacy t-shirts.',
                single_price=12500, monthly_price=5000,
                image_data='/static/images/fpo/yec-combo-tee-web.png',
                options=[
                    {"T23-RCF": {"T23-RCF-C01": "Small", "T23-RCF-C02": "Medium",
                                 "T23-RCF-C03": "Large", "T23-RCF-C04": "XL",
                                 "T23-RCF-C05": "2XL", "T23-RCF-C06": "3XL",
                                 "T23-RCF-C07": "4XL"}},
                    {"T24-RCF": {"T24-RCF-C01": "Small", "T24-RCF-C02": "Medium",
                                 "T24-RCF-C03": "Large", "T24-RCF-C04": "XL",
                                 "T24-RCF-C05": "2XL", "T24-RCF-C06": "3XL",
                                 "T24-RCF-C07": "4XL"}},
                    ],
            ),
            Perk(
                id="6ad405f9-7844-4cf8-9879-340c58dd54c8",
                name='hoodie',
                desc='Your generous support of Tor gets you this high quality zip hoodie.',
                single_price=50000, monthly_price=10000,
                image_data='/static/images/fpo/forever-hoodie.png',
                options=[
                    {"T25-RCF": {"T25-RCF-C01": "Small", "T25-RCF-C02": "Medium",
                                 "T25-RCF-C03": "Large", "T25-RCF-C04": "XL",
                                 "T25-RCF-C05": "2XL", "T25-RCF-C06": "3XL",
                                 "T25-RCF-C07": "4XL"}},
                    ],
            ),
        ],
        yec_total=1000000,
        is_yec=True,
        minimum_donation=200,
    )
    paypal: providers.Factory[PaypalController] = providers.Factory(
        PaypalController,
        settings.PAYPAL_CLIENT_ID,
        settings.PAYPAL_APP_SECRET,
        settings.PAYPAL_SUBSCRIPTION_PRODUCT_ID,
        settings.PAYPAL_WEBHOOK_ID,
        civi=civi,
    )
    stripe: providers.Factory[StripeController] = providers.Factory(
        StripeController,
        settings.STRIPE_API_KEY,
        settings.STRIPE_API_SECRET,
        settings.STRIPE_WEBHOOK_SECRET,
        civi=civi,
    )
