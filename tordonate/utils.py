import typing as t
from asyncio import iscoroutinefunction

from asgiref.sync import async_to_sync

if t.TYPE_CHECKING:
    from .forms import DonorInfoForm

_with_sync_sentinel = object()

_CallableT = t.TypeVar('_CallableT', bound=t.Callable)
_T = t.TypeVar('_T')


def with_sync(f: _CallableT) -> _CallableT:
    f.__with_sync__ = _with_sync_sentinel  # type: ignore
    return f


class WithSyncMeta(type):
    """Add synchronous versions of `@with_sync` methods to a class.

    A method must be decorated with `with_async` in order for a sync method to be generated.
    Generated sync methods have the name `{async_method_name}_sync`. Example:

    ```
    class Foo(metaclass=WithSyncMeta):
        @with_sync
        async def bar(self):
            await baz()
    ```

    Will be transformed into the equivalent of:

    ```
    class Foo:
        async def bar(self):
            await baz()

        def bar_sync(self):
            asgiref.sync.async_to_sync(baz)()
    ```
    """

    def __new__(
        cls: type[_T],
        name: str,
        bases: t.Iterable[str],
        dct: dict[str, t.Any],
    ) -> type[_T]:
        new_class: type[_T] = t.cast(
            type[_T], super().__new__(cls, name, bases, dct))  # type: ignore

        sync_wrappers = {}

        for base in reversed(new_class.__mro__):
            for property_name, property_value in base.__dict__.items():
                if (
                    iscoroutinefunction(property_value)
                    and getattr(property_value, '__with_sync__', None) is _with_sync_sentinel
                ):
                    sync_wrappers[f'{property_name}_sync'] = async_to_sync(property_value)

        for sync_name, sync_wrapper in sync_wrappers.items():
            setattr(new_class, sync_name, sync_wrapper)

        return new_class


class WithSyncProtocol(type(t.Protocol), WithSyncMeta):  # type: ignore
    pass


def create_order_summary(form: 'DonorInfoForm') -> dict[str, t.Any]:
    keys = [
        'first_name',
        'last_name',
        'single_or_monthly',
        'donation_amount',
        'no_gift',
        'perk',
        'street_address',
        'apartment_number',
        'country',
        'city',
        'state',
        'postal_code',
        'email_address',
        'email_updates',
    ]

    return {key: form.data.get(key) for key in keys}
