import json
from datetime import datetime

from async_stripe import stripe
from django.http import HttpRequest

from ..civicrm.repository import CivicrmRepositoryMock


class StripeController:
    def __init__(
        self,
        api_key: str,
        api_secret: str,
        webhook_secret: str,
        civi: CivicrmRepositoryMock
    ):
        self.__api_key = api_key
        self.__api_secret = api_secret
        self.__webhook_secret = webhook_secret
        self._civi = civi

        self.serviceErrorDisplayMessage = """An error occurred while processing your card.
Please wait and try your donation again later.
If this error persists, please email admin@torproject.net."""

    def stripe_exception_handler(self, e: Exception) -> None:
        if type(e) is stripe.error.CardError:
            # TODO: Determine whether to log this
            pass
        elif type(e) is stripe.error.RateLimitError:
            # TODO: Log rate limiting event
            pass
        elif type(e) is stripe.error.InvalidRequestError:
            # TODO: Log event
            pass
        elif type(e) is stripe.error.AuthenticationError:
            # TODO: Log authentication issue with supplied API key
            pass
        elif type(e) is stripe.error.APIConnectionError:
            # TODO: Log trouble connecting with API
            pass
        elif type(e) is stripe.error.StripeError:
            # TODO: Log error and flag via email or similar
            pass
        else:
            # Something else happened, completely unrelated to Stripe
            pass
        return

    async def create_payment_intent(self, price: int) -> stripe.PaymentIntent:
        stripe.api_key = self.__api_secret
        try:
            return await stripe.PaymentIntent.create(
                amount=price,
                currency="usd",
            )
        except Exception as e:
            self.stripe_exception_handler(e)
            raise Exception(self.serviceErrorDisplayMessage)

    async def update_payment_intent(
        self,
        payment_id: str,
        price: int
    ) -> stripe.PaymentIntent:
        stripe.api_key = self.__api_secret
        try:
            return await stripe.PaymentIntent.modify(
                payment_id,
                amount=price,
            )
        except Exception as e:
            self.stripe_exception_handler(e)
            raise Exception(self.serviceErrorDisplayMessage)

    async def get_or_create_monthly_plan(self, price: int) -> stripe.Plan:
        stripe.api_key = self.__api_secret
        plan_id = f'web_donation_monthly_usd_{price}'
        # Stripe's SDK will throw an InvalidRequestError in the event that the
        # plan_id being passed to stripe.Plan.retrieve() has no matches.
        # In this event, we create a new plan using a service product.
        # (Other error types thrown by Stripe's SDK are handled as usual.)
        # API reference: https://docs.stripe.com/api/plans/create
        try:
            plan = await stripe.Plan.retrieve(plan_id)
            return plan
        except stripe.error.InvalidRequestError:
            try:
                plan = await stripe.Plan.create(
                    amount=price,
                    currency='usd',
                    interval='month',
                    product={
                        "name": plan_id,
                    },
                )
                return plan
            except stripe.error.StripeError as e:
                self.stripe_exception_handler(e)
                raise Exception(self.serviceErrorDisplayMessage)
        except stripe.error.StripeError as e:
            self.stripe_exception_handler(e)
            raise Exception(self.serviceErrorDisplayMessage)

    async def create_customer(self) -> stripe.Customer:
        stripe.api_key = self.__api_secret
        try:
            return await stripe.Customer.create()
        except Exception as e:
            self.stripe_exception_handler(e)
            raise Exception(self.serviceErrorDisplayMessage)

    async def create_subscription(self, customer_id: str, price_id: str) -> stripe.Subscription:
        stripe.api_key = self.__api_secret
        try:
            return await stripe.Subscription.create(
                customer=customer_id,
                items=[{'price': price_id}],
                payment_behavior='default_incomplete',
                payment_settings={'save_default_payment_method': 'on_subscription'},
                expand=['latest_invoice.payment_intent'],
            )
        except Exception as e:
            self.stripe_exception_handler(e)
            raise Exception(self.serviceErrorDisplayMessage)

    async def send_donation_info_to_crm(self, recurring: bool, donationInfo: dict) -> None:
        if recurring:
            message = 'Tor\\Donation\\RecurringContributionSetup'
        else:
            message = ('Tor\\Donation\\OneTimeContribution')

        try:
            await self._civi.report_donation(message, donationInfo)
        except (TypeError, ValueError) as e:
            raise e
        return

    async def validate_webhook(self, request: HttpRequest) -> None:
        payload = request.body
        sig_header = request.META['HTTP_STRIPE_SIGNATURE']

        try:
            stripe.Webhook.construct_event(
                payload, sig_header, self.__webhook_secret
            )
        except ValueError as e:
            print('Error parsing payload: {}'.format(str(e)))
            raise Exception
        except stripe.error.SignatureVerificationError as e:
            print('Error verifying webhook signature: {}'.format(str(e)))
            raise Exception
        return

    async def process_webhook(self, request: HttpRequest) -> None:
        payload = request.body
        event = json.loads(payload)
        message = "Tor\\Donation\\RecurringContributionOngoing"
        receipt_time = datetime.utcfromtimestamp(event['created']).strftime('%Y-%m-%d %H:%M:%S')
        args = {
            'currency': 'USD',
            'payment_instrument_id': 'Credit Card',
            'receive_date': receipt_time,
            'total_amount': event['data']['object']['amount'],
            'trxn_id': event['id'],
            # 'recurring_contribution_transaction_id': event['data']['object']['subscription'],
            'recurring_contribution_transaction_id': 12345,
            'contribution_status_id': ''
        }
        match event['type']:
            case "invoice.payment_succeeded":
                args['contribution_status_id'] = 'Completed'
            case "invoice.payment_failed":
                args['contribution_status_id'] = 'Failed'

        try:
            await self._civi.report_donation(message, args)
        except (TypeError, ValueError) as e:
            raise e

        return
