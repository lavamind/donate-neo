from django.urls import path

from . import views

urlpatterns = [
    path('create_payment_intent/', views.create_payment_intent, name='create_payment_intent'),
    path('update_payment_intent/', views.update_payment_intent, name='update_payment_intent'),
    path('process/', views.process, name='process'),
    path('webhook/', views.webhook, name='webhook'),
]
