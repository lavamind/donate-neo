import asyncio
import json
import typing as t

from dependency_injector.wiring import Provide, inject
from django.http import HttpRequest, HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from ..containers import Container
from .forms import StripeDonationForm

if t.TYPE_CHECKING:
    from ..civicrm.repository import CivicrmRepositoryProtocol
    from .controller import StripeController


@inject
async def create_payment_intent(
    request: HttpRequest,
    stripe: 'StripeController' = Provide[Container.stripe],
) -> HttpResponse:
    if request.method != 'POST':
        return HttpResponseNotAllowed()
    data = json.loads(request.body)

    try:
        intent = await stripe.create_payment_intent(data["donation"])
    except Exception as e:
        return JsonResponse({"error": str(e)})

    return JsonResponse({
        "payment_id": intent['id'],
        "client_secret": intent['client_secret']
    }, safe=False)


@inject
async def update_payment_intent(
    request: HttpRequest,
    stripe: 'StripeController' = Provide[Container.stripe],
) -> HttpResponse:
    if request.method != 'POST':
        return HttpResponseNotAllowed()
    data = json.loads(request.body)

    try:
        intent = await stripe.update_payment_intent(
            data["payment_id"],
            data["donation"]
        )
    except Exception as e:
        return JsonResponse({"error": str(e)})

    return JsonResponse({
        "client_secret": intent['client_secret']
    }, safe=False)


@inject
async def process(
    request: HttpRequest,
    stripe: 'StripeController' = Provide[Container.stripe],
    civi: 'CivicrmRepositoryProtocol' = Provide[Container.civi]
) -> HttpResponse:
    if request.method != 'POST':
        return HttpResponseNotAllowed()
    data = json.loads(request.body)
    form = StripeDonationForm(data=data)

    if not form.is_valid():
        return JsonResponse({"error": form.errors.as_json()})

# Due to the way that Stripe differentiates between a one-time payment and
# a subscription, we instantiate subscriptions now, after the form has been
# validated and the recurring donation amount has been decided upon by the
# donor. This action creates a client secret, which one-time payments are
# able to create right after pageload. Therefore, we return a type value
# to allow the front-end to differentiate usefully between success payloads
# along with subscriptions' client secrets.
    if form.cleaned_data['recurring']:
        try:
            plan = await stripe.get_or_create_monthly_plan(form.cleaned_data['amount'])
            customer = await stripe.create_customer()
            sub = await stripe.create_subscription(customer.id, plan.id)
            return JsonResponse({
                "type": "subscription",
                "client_secret": sub.latest_invoice.payment_intent.client_secret
            })
        except Exception as e:
            return JsonResponse({"error": str(e)})

    return JsonResponse({"type": "payment"})


# This route handles incoming webhook requests originating from Paypal servers.
# Because these requests don't originate from our server, no CSRF token is
# generated, and therefore this method receives the rare CSRF exemption.
# In exchange, we explicitly verify this webhook's legitimacy via the code in
# stripe.validate_webhook().
@inject
@csrf_exempt
def webhook(
    request: HttpRequest,
    stripe: 'StripeController' = Provide[Container.stripe],
) -> HttpResponse:
    try:
        asyncio.run(stripe.validate_webhook(request))
    except Exception:
        return HttpResponse(status=400)

# When Stripe issues requests to our webhook endpoint, it interprets
# any HTTP response code in the 2xx range as a success, and anything
# else as a failure. (It will reissue the same request several times
# if it believes a request it has issued has failed.) There is no
# nuance in how it interprets failures, so we simply issue - as their
# webhook code samples suggest - a 400 Bad Request if exceptions
# are encountered, and 200 OK if all goes well.

    try:
        asyncio.run(stripe.process_webhook(request))
    except Exception:
        return HttpResponse(status=400)

    return HttpResponse(status=200)
