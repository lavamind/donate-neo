import asyncio
import json
import typing as t

from dependency_injector.wiring import Provide, inject
from django.http import HttpRequest, HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from ..containers import Container

if t.TYPE_CHECKING:
    from .controller import PaypalController


@inject
async def create_order(
    request: HttpRequest,
    paypal: 'PaypalController' = Provide[Container.paypal]
) -> HttpResponse:
    if request.method != 'POST':
        return HttpResponseNotAllowed()

    data = json.loads(request.body)
    try:
        order = await paypal.create_order(data['payment_amount'])
        return JsonResponse(order)
    except Exception as e:
        return JsonResponse({"error": str(e)})


@inject
async def capture_order_payment(
    request: HttpRequest,
    paypal: 'PaypalController' = Provide[Container.paypal]
) -> HttpResponse:
    if request.method != 'POST':
        return HttpResponseNotAllowed()

    data = json.loads(request.body)
    try:
        order = await paypal.capture_payment(data['order_id'])
        return JsonResponse(order)
    except Exception as e:
        return JsonResponse({"error": str(e)})


@inject
async def create_subscription(
    request: HttpRequest,
    paypal: 'PaypalController' = Provide[Container.paypal]
) -> HttpResponse:
    if request.method != 'POST':
        return HttpResponseNotAllowed()

    data = json.loads(request.body)

    try:
        product = await paypal.get_or_create_product()
        subscription = await paypal.create_subscription(
            data['payment_amount'],
            product['id'],
        )
        return JsonResponse(subscription)
    except Exception as e:
        return JsonResponse({"error": str(e)})


# This route handles incoming webhook requests originating from Paypal servers.
# Because these requests don't originate from our server, no CSRF token is
# generated, and therefore this method receives the rare CSRF exemption.
# In exchange, we explicitly verify this webhook's legitimacy via the code in
# paypal.validate_webhook().
@inject
@csrf_exempt
def webhook(
    request: HttpRequest,
    paypal: 'PaypalController' = Provide[Container.paypal]
) -> HttpResponse:
    if request.method != 'POST':
        return HttpResponseNotAllowed()
    payload = request.body

    try:
        parsed_request_body = json.loads(payload)
    except json.decoder.JSONDecodeError:
        return HttpResponse(status=400)

# When Paypal issues requests to our webhook endpoint, it interprets
# a 200 OK HTTP response code as a success, and anything else as a
# failure. (It will reissue the same request several times if it
# believes a request it has issued has failed.) There is no nuance
# in how it interprets failures, so we simply issue - as their
# webhook code samples suggest - a 400 Bad Request if exceptions
# are encountered, and 200 OK if all goes well.

    try:
        verify = asyncio.run(paypal.validate_webhook(request.headers, parsed_request_body))
    except Exception:
        return HttpResponse(status=400)

    if verify['verification_status'] == "SUCCESS":
        try:
            asyncio.run(paypal.process_webhook(parsed_request_body))
        except Exception:
            return HttpResponse(status=400)

    return HttpResponse(status=200)
