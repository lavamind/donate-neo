from django.urls import path

from . import views

urlpatterns = [
    path('create-order/', views.create_order, name='create_order'),
    path('capture-order-payment/', views.capture_order_payment, name='capture_order_payment'),
    path('create-subscription/', views.create_subscription, name='create_subscription'),
    # path('capture-subscription/', views.capture_subscription, name='capture_subscription'),
    path('webhook/', views.webhook, name='webhook'),
]
