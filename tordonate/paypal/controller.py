import re
import typing as t
from base64 import b64encode
from datetime import datetime

import dateutil.parser as dateparser
import requests

from ..civicrm.repository import CivicrmRepositoryMock


class PaypalController:

    def __init__(
        self,
        client_id: str,
        app_secret: str,
        sub_product_id: str,
        webhook_id: str,
        civi: CivicrmRepositoryMock,
        base_url: str = 'https://api-m.sandbox.paypal.com',
    ):
        self.__client_id = client_id
        self.__app_secret = app_secret
        self.__sub_product_id = sub_product_id
        self.__webhook_id = webhook_id
        self._civi = civi
        self.base_url = base_url

        self.serviceErrorDisplayMessage = """An error occurred while processing your card.
Please wait and try your donation again later.
If this error persists, please email admin@torproject.net."""

    # Paypal's API requires that currency values be passed with decimal precision.
    # Because both CiviCRM and Stripe want donation values in "number of cents,"
    #   we use this method to ensure incoming values have been reformatted to x.xx USD.
    # Visit https://developer.paypal.com/docs/api/orders/v2/ for more information;
    # navigate to Request Body Schema > purchase_units > amount > value
    def isPaymentPaypalFormatted(self, payment: str) -> bool:
        return bool(re.match(r"\d+\.\d{2}", payment))

    # API reference:
    # https://developer.paypal.com/api/rest/authentication/
    async def generate_access_token(self) -> str:
        auth_token = b64encode(f'{self.__client_id}:{self.__app_secret}'.encode()).decode()
        try:
            response = requests.post(
                f'{self.base_url}/v1/oauth2/token',
                data={'grant_type': 'client_credentials'},
                headers={'Authorization': f'Basic {auth_token}'},
            )
            response.raise_for_status()
            return t.cast(str, response.json()['access_token'])

        except requests.HTTPError:
            print(response.status_code, response.reason)
            raise Exception(self.serviceErrorDisplayMessage)

    # API reference:
    # https://developer.paypal.com/docs/api/orders/v2/
    async def create_order(self, payment_amount: str) -> dict[str, t.Any]:
        if not self.isPaymentPaypalFormatted(payment_amount):
            print(str("Payment malformed."))
            raise Exception(self.serviceErrorDisplayMessage)

        access_token = await self.generate_access_token()
        try:
            response = requests.post(
                f'{self.base_url}/v2/checkout/orders',
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': f'Bearer {access_token}',
                },
                json={
                    'intent': 'CAPTURE',
                    'purchase_units': [
                        {
                            'amount': {
                                'currency_code': 'USD',
                                'value': payment_amount,
                            },
                        },
                    ],
                },
            )
            response.raise_for_status()
            return {"id": response.json()["id"]}
        except requests.HTTPError:
            print(response.status_code, response.reason)
            raise Exception(self.serviceErrorDisplayMessage)

    # API reference:
    # https://developer.paypal.com/docs/api/orders/v2/#orders_capture
    async def capture_payment(self, order_id: str) -> dict[str, t.Any]:
        access_token = await self.generate_access_token()
        try:
            response = requests.post(
                f'{self.base_url}/v2/checkout/orders/{order_id}/capture',
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': f'Bearer {access_token}',
                },
            )
            response.raise_for_status()
            data: dict[str, t.Any] = response.json()
        except requests.HTTPError:
            print(response.status_code, response.reason)
            raise Exception(self.serviceErrorDisplayMessage)

        return data

    # API reference:
    # https://developer.paypal.com/docs/api/catalog-products/v1/#products_get
    async def verify_product(self, product_id: str) -> bool:
        access_token = await self.generate_access_token()
        try:
            response = requests.get(
                f'{self.base_url}/v1/catalogs/products/{product_id}',
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': f'Bearer {access_token}',
                },
            )
            response.raise_for_status()
            return True
        except requests.HTTPError:
            print(response.status_code, response.reason)
            raise Exception(self.serviceErrorDisplayMessage)

    # API reference:
    # https://developer.paypal.com/docs/api/catalog-products/v1/#products_create
    async def get_or_create_product(self) -> dict[str, t.Any]:
        try:
            product_id = await self.verify_product(self.__sub_product_id)
        except Exception as e:
            print(str(e))
            raise Exception(self.serviceErrorDisplayMessage)

        if product_id is True:
            return {"id": self.__sub_product_id}

        access_token = await self.generate_access_token()
        try:
            response = requests.post(
                f'{self.base_url}/v1/catalogs/products',
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': f'Bearer {access_token}',
                },
                json={
                    'id': self.__sub_product_id,
                    'type': 'DIGITAL',
                    'name': 'Monthly recurring donation',
                },
            )
            response.raise_for_status()
            data: dict[str, t.Any] = response.json()
            return {"id": data["id"]}

        except requests.HTTPError:
            print(response.status_code, response.reason)
            raise Exception(self.serviceErrorDisplayMessage)

    # API reference:
    # https://developer.paypal.com/docs/api/subscriptions/v1/#plans_create
    async def create_subscription(self, payment_amount: str, id: str) -> dict[str, t.Any]:
        if not self.isPaymentPaypalFormatted(payment_amount):
            raise Exception("Payment malformed.")

        access_token = await self.generate_access_token()
        try:
            response = requests.post(
                f'{self.base_url}/v1/billing/plans',
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': f'Bearer {access_token}',
                },
                json={
                    'product_id': id,
                    'name': f'{payment_amount} recurring payment',
                    'billing_cycles': [
                        {
                            'tenure_type': 'REGULAR',
                            'sequence': 1,
                            "frequency": {
                                "interval_unit": "MONTH",
                                "interval_count": 1
                            },
                            'total_cycles': 0,
                            "pricing_scheme": {
                                "fixed_price": {
                                    "currency_code": "USD",
                                    "value": payment_amount
                                },
                            },
                        },
                    ],
                    'payment_preferences': {
                        'auto_bill_outstanding': 'true',
                        'payment_failure_threshold': 3,
                        'setup_fee_failure_action': 'CONTINUE',
                        'setup_fee': {
                            'currency_code': 'USD',
                            'value': 0,
                        },
                    },
                },
            )
            response.raise_for_status()
            data: dict[str, t.Any] = response.json()
            return {"id": data["id"]}

        except requests.HTTPError:
            print(response.status_code, response.reason)
            raise Exception(self.serviceErrorDisplayMessage)

    # Because incoming webhook data is CSRF-exempt and therefore more prone to
    # potential malfeasance, we verify all incoming requests to this route with
    # Paypal. Note that self.__webhook_id is not passed to us from the incoming
    # request, but instead generated on the Paypal developer dashboard and
    # stored with the rest of our private keys.
    # API reference:
    # https://developer.paypal.com/docs/api/webhooks/v1/#verify-webhook-signature
    async def validate_webhook(self, headers: dict, body: dict) -> dict:
        access_token = await self.generate_access_token()
        response = requests.post(
            f'{self.base_url}/v1/notifications/verify-webhook-signature',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {access_token}',
            },
            json={
                'transmission_id': headers['paypal-transmission-id'],
                'transmission_time': headers['paypal-transmission-time'],
                'cert_url': headers['paypal-cert-url'],
                'auth_algo': headers['paypal-auth-algo'],
                'transmission_sig': headers['paypal-transmission-sig'],
                'webhook_id': self.__webhook_id,
                'webhook_event': body,
            },
        )
        data: dict[str, t.Any] = response.json()
        return data

    async def process_webhook(self, event: dict) -> None:
        # Paypal webhook timestamps are ISO 8601-formatted with a trailing Z,
        # which Python's own datetime library does not support (despite its
        # fairly-comprehensive parser support for other ISO 8601 formats).
        # Ergo, we use python-dateutil to parse timestamps here.
        receipt_parsed = dateparser.parse(event['create_time'])
        receipt_timestamp = receipt_parsed.timestamp()
        receipt_time = datetime.utcfromtimestamp(receipt_timestamp).strftime('%Y-%m-%d %H:%M:%S')
        message = "Tor\\Donation\\RecurringContributionOngoing"

        args = {
            'payment_instrument_id': 'PayPal',
            'receive_date': receipt_time,
            'trxn_id': event['resource']['id'],
        }

        match event['event_type']:
            case "PAYMENT.SALE.COMPLETED":
                args['currency'] = event['resource']['amount']['currency']
                args['total_amount'] = event['resource']['amount']['total']
                args['contribution_status_id'] = 'Completed'
            case "BILLING.SUBSCRIPTION.PAYMENT.FAILED":
                args['currency'] = \
                    event['resource']['billing_info']['outstanding_balance']['currency_code']
                args['total_amount'] = \
                    event['resource']['billing_info']['outstanding_balance']['value']
                args['contribution_status_id'] = 'Failed'

        try:
            await self._civi.report_donation(message, args)
        except (TypeError, ValueError) as e:
            raise e

        return
