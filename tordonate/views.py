# mypy: disable-error-code=no-untyped-def
import json

from dependency_injector.wiring import Provide, inject
from django.conf import settings
from django.http import HttpRequest, HttpResponse, HttpResponseNotAllowed, JsonResponse, QueryDict
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from .containers import Container
from .forms import CryptocurrencyForm, DonorInfoForm
from .utils import create_order_summary


class DonateFormView(FormView):
    form_class = DonorInfoForm
    template_name = 'donate.html.jinja'
    success_url = reverse_lazy('donate_thank_you')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'donate_single_prices': settings.DONATE_PRICES['single'],
            'donate_monthly_prices': settings.DONATE_PRICES['monthly'],
            'paypal_client_id': settings.PAYPAL_CLIENT_ID,
            'stripe_api_key': settings.STRIPE_API_KEY,
            'crypto_wallets': settings.CRYPTO_WALLET_ADDRESSES
        })

        return context

    @inject
    def form_valid(self, form: DonorInfoForm, civi=Provide[Container.civi]):
        self.request.session['order_summary'] = create_order_summary(form)
        civi.report_donation_sync(form)
        if form.cleaned_data['email_updates']:
            civi.newsletter_signup_sync(form.cleaned_data['email_address'])

        return super().form_valid(form)

    @inject
    def ajax_validate(
        request: HttpRequest,
    ) -> HttpResponse:
        if request.method != 'POST':
            return HttpResponseNotAllowed()
        jsonData = json.loads(request.body)
        data = QueryDict(jsonData["form"].encode('ASCII'))
        form = DonorInfoForm(data)

        if form.is_valid():
            return JsonResponse({})
        else:
            return JsonResponse({"errors": form.errors})


class CryptocurrencyView(FormView):
    form_class = CryptocurrencyForm
    template_name = 'cryptocurrency.html.jinja'
    success_url = reverse_lazy('donate_thank_you')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'donate_single_prices': settings.DONATE_PRICES['single'],
            'crypto_wallets': settings.CRYPTO_WALLET_ADDRESSES,
            'btcpayserver_url': settings.BTCPAYSERVER_URL
        })
        return context


class DonateThankYouView(TemplateView):
    template_name = 'donate_thank_you.html.jinja'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'order_summary': self.request.session.pop('order_summary', {}),
            'stripe_api_key': settings.STRIPE_API_KEY,
        })
        return context


class FAQView(TemplateView):
    template_name = 'faq.html.jinja'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
