import datetime
import typing as t

from captcha.fields import CaptchaField, CaptchaTextInput
from dependency_injector.wiring import Provide, inject
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.utils.translation import gettext as _

from . import container
from .civicrm.models import Perk
from .civicrm.repository import CivicrmRepositoryProtocol
from .containers import Container


class CaptchaBetter(CaptchaTextInput):
    template_name = 'widgets/captcha_with_audio.html'


class PerkField(forms.ChoiceField):
    @inject
    def to_python(
        self,
        value: t.Any,
        civi: CivicrmRepositoryProtocol = Provide[Container.civi],
    ) -> t.Optional[Perk]:
        if type(value) is not str:
            return None

        return t.cast(t.Optional[Perk], civi.get_perk_by_id_sync(value))  # type: ignore

    def validate(self, value: t.Any) -> None:
        if value is None and not self.required:
            return

        if type(value) is Perk:
            for perk_id, __ in self.choices:
                if value.id == perk_id:
                    return

        raise ValidationError(
            self.error_messages["invalid_choice"],
            code="invalid_choice",
            params={"value": value},
        )


class DonorInfoForm(forms.Form):
    defaultDonation = {
        'single': settings.DEFAULT_DONATION["single"],
        'monthly': settings.DEFAULT_DONATION["monthly"],
    }
    minimum_donation = {
        "cents": container.civi().minimum_donation_sync(),  # type: ignore
        "decimal": '{0:.2f}'.format(
            container.civi().minimum_donation_sync() / 100),  # type: ignore
        "display": "$" + str(
            '{0:.2f}'.format(container.civi().minimum_donation_sync() / 100)),  # type: ignore
    }
    donation_amount = forms.IntegerField(
        required=True,
        validators=[
            MinValueValidator(container.civi().minimum_donation_sync()),  # type: ignore
        ],
    )
    single_or_monthly = forms.ChoiceField(
        choices=[('single', 'Give once'), ('monthly', 'Repeat monthly')],
        widget=forms.RadioSelect(),
        initial='single',
    )
    no_gift = forms.BooleanField(required=False, initial=True)
    perk = PerkField(
        widget=forms.RadioSelect(attrs={'class': 'btn-check', 'disabled': 'disabled'}),
        required=False,
        choices=lambda: [
            (perk.id, {
                "name": perk.name,
                "desc": perk.desc,
                "price_single": perk.single_price,
                "price_monthly": perk.monthly_price,
                "image_data": perk.image_data
            }) for perk in container.civi().get_active_perks_sync()  # type: ignore
        ],
    )
    options_by_perk = []
    for index, optionByPerk in enumerate(container.civi().get_active_perks_synced()):
        options_by_perk.append({
            "index": index,
            "id": optionByPerk.id,
            'name': optionByPerk.name,
            "options": optionByPerk.options
        })
    # TODO: once API is modeled to return option names and option dependencies,
    #       create select fields here rather than in template for proper
    #       backend validation

    first_name = forms.CharField(max_length=1000)
    first_name.widget = forms.TextInput(attrs={'class': 'mt-2 form-control'})

    last_name = forms.CharField(max_length=1000)
    last_name.widget = forms.TextInput(attrs={'class': 'mt-2 form-control'})

    street_address = forms.CharField(max_length=1000)
    street_address.widget = forms.TextInput(attrs={'class': 'mt-2 form-control'})

    apartment_number = forms.CharField(max_length=1000, required=False)
    apartment_number.widget = forms.TextInput(attrs={'class': 'mt-2'})

    country = forms.CharField(max_length=1000)
    country.widget = forms.Select(attrs={'class': 'mt-2 form-control', 'required': 'required'})

    city = forms.CharField(max_length=1000)
    city.widget = forms.TextInput(attrs={'class': 'mt-2 form-control'})

    state = forms.CharField(max_length=1000)
    state.widget = forms.Select(attrs={'class': 'mt-2 form-control', 'required': 'required'})

    postal_code = forms.CharField(max_length=1000)
    postal_code.widget = forms.TextInput(attrs={'class': 'mt-2 form-control'})

    email_address = forms.CharField(max_length=1000)
    email_address.widget = forms.EmailInput(attrs={'class': 'mt-2 mb-2 form-control'})

    email_updates = forms.BooleanField(required=False,
                                       label="Send me updates from the Tor Project")
    email_updates.widget = forms.CheckboxInput()

    comments = forms.CharField(max_length=1000, required=False)

    captcha = CaptchaField(widget=CaptchaBetter, label="")

    def clean(self) -> dict[str, t.Any]:
        errors = []
        cleaned_data = t.cast(dict[str, t.Any], super().clean())

        # pprint(cleaned_data)

        if cleaned_data.get('first_name') is None:
            errors.append(ValidationError(_('Please enter a first name.')))
        if cleaned_data.get('last_name') is None:
            errors.append(ValidationError(_('Please enter a last name.')))

        no_gift = cleaned_data.get('no_gift', False)
        donation_amount = cleaned_data.get('donation_amount')
        donation_type = cleaned_data.get('single_or_monthly')

        perk = self.cleaned_data.get('perk')

        if perk and donation_type == 'single':
            perk_minimum = perk.single_price
        elif perk and donation_type == 'monthly':
            perk_minimum = perk.monthly_price

        # validate single/monthly perk prices
        if perk and donation_amount and perk_minimum and donation_amount < perk_minimum:
            errors.append(ValidationError(
                _('Donation amount of %(donation_amount)s is less than the minimum donation of '
                  '%(perk_minimum)s for %(perk_name)s'),
                params={
                    'donation_amount': donation_amount,
                    'perk_minimum': perk_minimum,
                    'perk_name': perk.name}))

        # validate no gift/perk choices
        if perk is None and not no_gift:
            errors.append(ValidationError(
                _('No perk was selected, but the "No gift" option was not checked')))
        elif perk is not None and no_gift:
            errors.append(ValidationError(
                _('A perk was selected, but the "No gift" option was checked')))

        if len(errors):
            raise ValidationError(errors)
        return cleaned_data


class CryptocurrencyForm(forms.Form):
    defaultDonation = {
        'single': settings.DEFAULT_DONATION["single"],
        'monthly': settings.DEFAULT_DONATION["monthly"],
    }
    anonymous_donation = forms.BooleanField(required=False)
    first_name = forms.CharField(max_length=1000, required=False)
    last_name = forms.CharField(max_length=1000, required=False)
    email_opt_in = forms.BooleanField(required=False)
    email_address = forms.EmailField()
    estimated_donation_date = forms.DateField(widget=forms.SelectDateWidget())
    # XXX: this is gross, create a new function to get the choices
    currency_type = forms.ChoiceField(
        choices=[
            ('', 'Choose a Currency'),
            *[
                (ticker_symbol, f'{wallet_name} ({ticker_symbol})')
                for ticker_symbol, (wallet_name, address)
                in settings.CRYPTO_WALLET_ADDRESSES.items()
            ],
        ],
    )
    currency_amount = forms.IntegerField(validators=[MinValueValidator(0)])

    def clean_estimated_donation_date(self) -> None:
        estimated_donation_date = self.cleaned_data.get('estimated_donation_date')

        if (
            # XXX: i don't think this function will be called if estimated_donation_date wasn't
            # provided, but i'm not sure
            not estimated_donation_date
            or (estimated_donation_date - datetime.date.today()).days < 0
        ):
            raise ValidationError(_('Estimated donation date is in the past'))

    def clean(self) -> dict[str, t.Any]:
        cleaned_data = t.cast(dict[str, t.Any], super().clean())
        cleaned_data = self.validate_form(cleaned_data)
        return cleaned_data

    def validate_form(self, data: dict[str, t.Any]) -> dict[str, t.Any]:
        errors = []

        anonymous_donation = data.get('anonymous_donation', False)
        first_name = data.get('first_name', '').strip()
        last_name = data.get('last_name', '').strip()

        if anonymous_donation:
            data['first_name'] = ''
            data['last_name'] = ''
        elif not len(first_name) or not len(last_name):
            errors.append(ValidationError(_('First or last name not specified')))

        if len(errors):
            raise ValidationError(errors)

        return data
