from django.apps import AppConfig

from . import container


class TordonateConfig(AppConfig):
    name = "tordonate"

    def ready(self) -> None:
        container.wire(modules=[
            '.forms', '.middleware', '.views',
            '.stripe.views', '.stripe.controller',
            '.paypal.views',
            '.civicrm.forms'])
