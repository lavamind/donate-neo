FROM containers.torproject.org/tpo/tpa/base-images:bookworm

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100

# Python dependencies
RUN apt-get update && \
  apt-get install -y --no-install-recommends \
    python-is-python3 \
    python3-pip \
    python3-venv && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Create unprivileged tordonate user/group
RUN groupadd -r -g 999 tordonate && \
  useradd --no-log-init -r -m -u 999 -g tordonate tordonate

# Work in application directory
WORKDIR /home/tordonate/app

# Copy all project files (minus those ignored)
COPY --chown=tordonate:tordonate . .

# Prepare environment for running commands in virtualenv
ENV PATH="/home/tordonate/venv/bin:$PATH" \
 VIRTUAL_ENV=/home/tordonate/venv

# Create virtualenv and install poetry+tordonate
# then make home directory owned by the right user
RUN --mount=type=cache,target=/root/.cache/pypoetry/cache \
  --mount=type=cache,target=/root/.cache/pypoetry/artifacts \
  --mount=type=cache,target=/root/.cache/pip \
  python3 -m venv /home/tordonate/venv && \
  pip install poetry && \
  poetry install --without=dev --no-interaction --no-ansi && \
  poetry run ./manage.py makemigrations && \
  poetry run ./manage.py migrate && \
  poetry run ./manage.py collectstatic --no-input && \
  chown tordonate:tordonate -R /home/tordonate

# Expose port 8000/tcp for gunicorn
EXPOSE 8000

# Switch to tordonate user
USER tordonate

# Run tordonate
CMD ["poetry", "run", "./manage.py", "runserver", "0.0.0.0:8000"]
