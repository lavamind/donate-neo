# flake8: noqa

"""
Django settings for tordonate that contains sensitive information,
like API keys.
"""

PAYPAL_CLIENT_ID = ''
PAYPAL_APP_SECRET = ''
PAYPAL_SUBSCRIPTION_PRODUCT_ID = ''
PAYPAL_WEBHOOK_ID = ''
STRIPE_API_KEY = ''
STRIPE_API_SECRET = ''

REDIS_SERVER = ''
REDIS_PORT = 0
REDIS_USERNAME = ''
REDIS_PASSWORD = ''
REDIS_DB = 0